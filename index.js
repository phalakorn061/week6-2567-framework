const http = require("http");
const app = http.createServer( (req, res) => {
    // (req, res) ชื่ออะไรก็ได้
    if (req.url === '/') {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.write("<h2>Home Page</h2>");
        res.end();
    } else if (req.url === "/about") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.write("About Page");
        res.end();
    } else if (req.url === "/admin") {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.write("<h1>Admin Page</h1>");
        res.end();
    } else {
         res.writeHead(200, { "Content-Type": "text/html" });
         res.write("<h1 style = 'color : red'><center>404</center></h1>");
         res.end();
    }
});

const PORT = 3000;

app.listen(PORT,  () => {
  console.log(`Server running at http://hostname:${PORT}`);
});

